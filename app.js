//****************CREATE SERVER***************//
const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
//const socketIO = require("./Socket/socket_manager");
//****************CREATE EXPRESS APP***************//

// const cors = require("cors");
// const fs = require("fs");

//const Logger = require("./Utilities/logger.js");
const appconfig = require("./config/appconfig.js");
const Logger = require("./lib/Utilities/logger.js");
const sequelizeORM = require("./lib/Database/index.js");
const path = require("path");
const { upload } = require("./lib/Utilities/upload_parser.js");
const { verifyToken } = require("./lib/Utilities/jwt_token.js");

//****************SET BASE PATH***************//
global.__basedir = __dirname;

//****************SETTING CORS***************//
// app.use(cors({
//     origin: '*'
// }));

//****************Attach Server To Socket***************//
//socketIO.attach(server);

//**************** CREATE DATABASE ***************//
sequelizeORM.sync();

//****************SET METHODS***************//
// app.set("views", __dirname + "/View");
// app.engine("html", require("ejs").renderFile);
// app.set("view engine", "ejs");

//****************USE METHODS***************//

//*********USE PARSERS*********//
app.use(express.json()); // Used to parse JSON bodies
app.use(express.urlencoded({ extended: true }));
app.use(upload.any());

//*********SET STATIC PATHS*********//
app.use("/images", express.static(appconfig.public.images));
app.use("/web", express.static(appconfig.public.web));
app.use("/uploads", express.static(appconfig.public.uploadPath));

//*********VERIFY TOKEN*********//
app.use("/auth", verifyToken);

//*********USE API ROUTES*********//
app.use(require("./lib/router"));

//*********USE LOGGER*********//
const logger = new Logger();
app.use((req, res, next) => {
  logger.log(
    "the url you are trying to reach is not hosted on our server",
    "error"
  );
  const err = new Error("Not Found");
  err.status = 404;
  res.status(err.status).json({
    type: "error",
    message: "the url you are trying to reach is not hosted on our server",
  });
  next();
});

//****************START SERVER***************//
server.listen(appconfig.app.port, () => {
  console.log(`Server is running at ${appconfig.app.base_url}`);
});
