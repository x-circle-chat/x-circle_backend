var nodemailer = require("nodemailer");
var mailerhbs = require("nodemailer-express-handlebars");
const appconfig = require("../../config/appconfig");

var transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "purigoswami30@gmail.com",
    pass: "hweifxvqwudcatvj",
  },
});

transporter.use(
  "compile",
  mailerhbs({
    viewEngine: {
      extName: ".hbs",
      partialsDir: appconfig.public.emails,
      defaultLayout: false,
    },
    viewPath: appconfig.public.emails, //Path to email template folder
    extName: ".hbs", //extendtion of email template
  })
);

module.exports.sendOTP = (email, username, otp) => {
  const appName = appconfig.app.appName;
  const test = "TEST";

  var mailOptions = {
    from: "purigoswami30@gmail.com",
    to: email,
    subject: "Verify OTP",
    template: "verify_otp",
    context: {
      appName: appName,
      username: username,
      otp: otp,
    },
    attachments: [
      {
        filename: "app_logo.png",
        path: appconfig.public.images + "/app_logo.png",
        cid: "logo", //my mistake was putting "cid:logo@cid" here!
      },
    ],
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
};
