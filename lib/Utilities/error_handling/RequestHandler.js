const _ = require("lodash");

class RequestHandler {
  constructor(logger) {
    this.logger = logger;
  }

  throwError(status, errorType, errorMessage) {
    return (e) => {
      if (!e) e = new Error(errorMessage || "Default Error");
      e.status = status;
      e.errorType = errorType;
      throw e;
    };
  }

  sendSuccess(res, message, status) {
    this.logger.log(
      `a request has been made and proccessed successfully at: ${new Date()}`,
      "info"
    );
    return (data) => {
      if (_.isUndefined(status)) {
        status = 200;
      }
      res.status(status).json({
        isSuccess: 1,
        message: message || "Success result",
        data,
      });
    };
  }

  sendError(req, res, error) {
    this.logger.log(
      `error ,Error during processing request: ${`${req.protocol}://${req.get(
        "host"
      )}${req.originalUrl}`} details message: ${error.message}`,
      "error"
    );
    return res.status(error.status || 500).json({
      isSuccess: 0,
      error_type: "error",
      message: error.message || error.errors[0].message || "Unhandled Error",
      error,
    });
  }

  sendCustomError(req, res, errorInfo) {
    this.logger.log(
      `error ,Error during processing request: ${`${req.protocol}://${req.get(
        "host"
      )}${req.originalUrl}`} details message: ${errorInfo.message}`,
      "error"
    );
    return res.status(errorInfo.status || 500).json({
      isSuccess: 0,
      error_type: errorInfo.errorType || "error",
      message: errorInfo.message,
    });
  }

  //Socket Error Handling
  sendSocketSuccess(completion, message) {
    return (data) => {
      completion({
        isSuccess: 1,
        message: message || "Success result",
        data,
      });
    };
  }

  sendSocketError(completion, error) {
    return completion({
      isSuccess: 0,
      message: error.message || "Unhandled Error",
      error,
    });
  }

  sendSocketCustomError(completion, errorInfo) {
    return completion({
      isSuccess: 0,
      message: errorInfo.message,
      error: errorInfo.errorType || "error",
    });
  }
}
module.exports = RequestHandler;
