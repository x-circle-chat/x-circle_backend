const { extend } = require("lodash");

module.exports.CustomError = {
  BadRequest(message) {
    return {
      errorType: "Bad Request",
      status: 400,
      get message() {
        return message || this.errorType;
      },
    };
  },

  Unauthorized(message) {
    return {
      errorType: "Unauthorised",
      status: 401,
      get message() {
        return message || this.errorType;
      },
    };
  },

  Forbidden(message) {
    return {
      errorType: "Forbidden",
      status: 403,
      get message() {
        return message || this.errorType;
      },
    };
  },

  NotFound(message) {
    return {
      errorType: "Not Found",
      status: 404,
      get message() {
        return message || this.errorType;
      },
    };
  },

  UnprocessableEntity(message) {
    return {
      errorType: 422,
      error: "Unprocessable Entity",
      get message() {
        return message || this.errorType;
      },
    };
  },

  InternalServerError(message) {
    return {
      errorType: "Internal Server Error",
      status: 500,
      get message() {
        return message || this.errorType;
      },
    };
  },

  onlyAdmin() {
    return extend({}, this.Forbidden(), {
      message: "Only admins are allowed to do this!",
    });
  },

  NoPermesssion() {
    return extend(
      {},
      {
        errorType: "Forbidden",
        status: 403,
        message: "You do not have permission to consume this resource!",
      }
    );
  },

  invalidId() {
    extend({}, this.BadRequest, {
      message: "Invalid Id parameter",
    });
  },

  invalidSearchTerm() {
    extend({}, this.BadRequest, {
      message: "Invalid search term",
    });
  },

  missingAttr(attrs) {
    return extend({}, this.BadRequest(), {
      message: `Attribute(s) (${attrs.join(",")}) seem(s) to be missing`,
    });
  },

  unwantedAttr(attrs) {
    return extend({}, this.BadRequest, {
      message: `Attribute(s) (${attrs.join(",")}) can't be updated`,
    });
  },

  uniqueAttr(attrs) {
    return extend({}, this.BadRequest, {
      message: `Attribute(s) [${attrs.join(",")}] must be unique`,
    });
  },

  custom(msg) {
    return extend({}, this.BadRequest, {
      message: msg,
    });
  },

  // REST

  addFailure(itemName) {
    return extend({}, this.BadRequest, {
      message: `${itemName || "Item"} was not added`,
    });
  },

  deleteFailure() {
    return extend({}, this.BadRequest, {
      message: `${itemName || "Item"} was not deleted`,
    });
  },

  updateFailure() {
    return extend({}, this.BadRequest, {
      message: `${itemName || "Item"} was not updated`,
    });
  },

  addSuccess(itemName) {
    return extend({}, this.Success, {
      message: `${itemName || "Item"} added successfully`,
    });
  },

  deleteSuccess(itemName) {
    return extend({}, this.Success, {
      message: `${itemName || "Item"} deleted successfully`,
    });
  },

  updateSuccess(itemName) {
    return extend({}, this.Success, {
      message: `${itemName || "Item"} updated successfully`,
    });
  },

  empty: [],
};
