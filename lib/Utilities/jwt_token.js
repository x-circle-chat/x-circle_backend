const jwt = require("jsonwebtoken");
const appconfig = require("../../config/appconfig");
const sequelizeORM = require("../Database");

class JWTToken {
  createJWT(userObject) {
    let jwtSecretKey = appconfig.auth.jwt_secret;
    try {
      const token = jwt.sign(userObject, jwtSecretKey);
      return token;
    } catch (error) {
      throw error;
    }
  }

  async verifyToken(request, response, next) {
    let tokenHeaderKey = appconfig.auth.jwt_header_key;
    let jwtSecretKey = appconfig.auth.jwt_secret;

    try {
      const token = request.header(tokenHeaderKey);

      if (token === "" || token === null) {
        let err = new Error("No token provided!");
        err.status = 403;
        response.send(err);
      } else {
        jwt.verify(token, jwtSecretKey, async (err, data) => {
          if (err) {
            if (err.name === "TokenExpiredError") {
              let err = new Error("You are not authenticated!");
              err.status = 401;
              response.send(err);
            } else {
              response.send(err);
            }
          } else {
            try {
              if (typeof data.id === "string") {
                const user = await sequelizeORM.models.user.findByPk(data.id);
                if (user) {
                  request.user = user;
                  next();
                } else {
                  response.send("User Not Exist");
                }
              } else {
                response.send("Invalid Token");
              }
            } catch (error) {
              response.send(error);
            }
          }
        });
      }
    } catch (error) {
      response.send(error);
    }
  }

  async varifyAndParseToken(token) {
    let jwtSecretKey = appconfig.auth.jwt_secret;

    try {
      if (token === "" || token === null) {
        let err = new Error("No token provided!");
        err.status = 403;
        throw err;
      } else {
        return jwt.verify(token, jwtSecretKey, (err, id) => {
          if (err) {
            throw err;
          } else {
            return id;
          }
        });
      }
    } catch (error) {
      throw error;
    }
  }
}

module.exports = new JWTToken();
