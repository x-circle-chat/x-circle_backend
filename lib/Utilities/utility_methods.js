exports.addMinutesToDate = (date, minutes) => {
  return new Date(date.getTime() + minutes * 60000);
};

exports.isValidString = (value) => {
  return value && typeof value == "string" && value != "";
};
