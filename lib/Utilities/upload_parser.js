const fs = require("fs");
const multer = require("multer");
const appconfig = require("../../config/appconfig");

var filestorageEngine = multer.diskStorage({
  destination: (req, file, cb) => {
    fs.mkdir(appconfig.public.uploadPath, (error) => {});
    cb(null, appconfig.public.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

exports.upload = multer({
  storage: filestorageEngine,
});
