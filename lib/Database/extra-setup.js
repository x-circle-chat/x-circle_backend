function applyExtraSetup(sequelize) {
  const { user, otp } = sequelize.models;

  //*** USER AND OTP TABLE ***//
  user.hasOne(otp, {
    foreignKey: {
      unique: true,
      allowNull: false,
    },
  });
  otp.belongsTo(user);
  //*************************//
}

module.exports = { applyExtraSetup };
