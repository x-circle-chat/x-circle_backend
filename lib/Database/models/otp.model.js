const { DataTypes } = require("sequelize");

module.exports = function (sequelize) {
  return sequelize.define(
    "otp",
    {
      id: {
        allowNull: false,
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      otp: DataTypes.STRING,
      expiration_time: DataTypes.DATE,
      created_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.fn("now"),
      },
      updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.fn("now"),
      },
    },
    {
      tableName: "otp",
    }
  );
};
