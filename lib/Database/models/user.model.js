const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  sequelize.define("user", {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: true,
      },
    },
    name: {
      type: DataTypes.STRING,
      defaultValue: "",
    },
    gender: {
      type: DataTypes.STRING,
      validate: {
        isIn: [["male", "female"]],
      },
    },
    birthday: {
      type: DataTypes.STRING,
      defaultValue: "",
    },
    profile_pic: {
      type: DataTypes.STRING,
      defaultValue: "",
    },
    isComplete: {
      type: DataTypes.INTEGER,
      defaultValue: false,
    },
    isVerified: {
      type: DataTypes.INTEGER,
      defaultValue: false,
    },
  });
};
