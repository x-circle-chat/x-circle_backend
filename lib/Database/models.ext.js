exports.updateOrCreate = async (model, where, newItem) => {
  // First try to find the record
  const foundItem = await model.findOne({ where });
  if (!foundItem) {
    // Item not found, create a new one
    const item = await model.create(newItem);
    return item;
  }
  // Found an item, update it
  await model.update(newItem, { where });

  //Updated Item
  const updatedItem = await model.findOne({ where });
  return updatedItem;
};
