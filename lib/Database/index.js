const { Sequelize } = require("sequelize");
const { applyExtraSetup } = require("./extra-setup");
const appconfig = require("../../config/appconfig");

//***** Create Sequelize *******
const sequelizeORM = new Sequelize(
  appconfig.dbConfig.database,
  appconfig.dbConfig.user,
  appconfig.dbConfig.password,
  {
    host: appconfig.host,
    dialect: "mysql",
    logQueryParameters: true,
    benchmark: true,
  }
);
//**************

console.log(sequelizeORM);

// ***** We define all models according to their files. *****
const modelDefiners = [
  require("./models/user.model"),
  require("./models/otp.model"),
];

for (const modelDefiner of modelDefiners) {
  modelDefiner(sequelizeORM);
}
//***************

//***** We execute any extra setup after the models are defined, such as adding associations. ********
applyExtraSetup(sequelizeORM);
//***************

//***** We export the sequelize connection instance to be used around our app. *******
module.exports = sequelizeORM;
