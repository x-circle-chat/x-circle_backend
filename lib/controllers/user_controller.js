const Logger = require("../Utilities/logger.js");
const { emit } = require("nodemon");
const RequestHandler = require("../Utilities/error_handling/RequestHandler.js");
const { CustomError } = require("../Utilities/error_handling/errors.js");
const appMessages = require("../commons/app_messages.js");
const sequelizeORM = require("../Database/index.js");
const { where } = require("sequelize");
const {
  addMinutesToDate,
  isValidString,
} = require("../Utilities/utility_methods.js");

const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class UserController {
  //🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹 LOGIN 🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹
  static async completeProfile(request, response) {
    //🌹🌹🌹 VALIDATE REQUEST 🌹🌹🌹
    const name = request.body.name;
    const gender = request.body.gender;
    const birthday = request.body.birthday;
    const profile_pic =
      request.files.length > 0 ? request.files[0].filename : "";

    if (
      !isValidString(name) ||
      !isValidString(gender) ||
      !isValidString(birthday) ||
      !isValidString(profile_pic)
    ) {
      return requestHandler.sendCustomError(
        request,
        response,
        CustomError.BadRequest(
          `Missing value: name ${name}, gender ${gender}, birthday ${birthday}, profile_pic ${profile_pic}}`
        )
      );
    }

    try {
      //🌹🌹🌹 UPDATE USER TABLE 🌹🌹🌹
      await sequelizeORM.models.user.update(
        {
          name: name,
          gender: gender,
          birthday: birthday,
          profile_pic: profile_pic,
          isComplete: 1,
        },
        { where: { id: request.user.id } }
      );

      //🌹🌹🌹 FTECH USER TABLE 🌹🌹🌹
      const user = await sequelizeORM.models.user.findByPk(request.user.id);

      //🌹🌹🌹 Success 🌹🌹🌹
      return requestHandler.sendSuccess(
        response,
        appMessages.success.profileCompleteSuccess
      )(user);
    } catch (error) {
      return requestHandler.sendError(request, response, error);
    }

    //Fallback Error
    return response.send(appMessages.error.someError);
  }

  //🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹

  //🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹 LOGIN 🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹
  static async getUser(request, response) {
    //🌹🌹🌹 CHECK USER ID 🌹🌹🌹
    let userID = request.body.id;

    if (!isValidString(userID)) {
      userID = request.user.id;
    }

    try {
      //🌹🌹🌹 FTECH USER TABLE 🌹🌹🌹
      const user = await sequelizeORM.models.user.findByPk(userID);

      //🌹🌹🌹 Success 🌹🌹🌹
      return requestHandler.sendSuccess(
        response,
        appMessages.success.getUser
      )(user);
    } catch (error) {
      return requestHandler.sendError(request, response, error);
    }

    //Fallback Error
    return response.send(appMessages.error.someError);
  }

  //🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹

  //🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹 LOGIN 🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹
  static async getUsers(request, response) {
    try {
      //🌹🌹🌹 FTECH USER TABLE 🌹🌹🌹
      const users = await sequelizeORM.models.user.findAll({
        where: {
          isComplete: 1,
        },
      });

      //🌹🌹🌹 Success 🌹🌹🌹
      return requestHandler.sendSuccess(
        response,
        appMessages.success.getUsers
      )(users);
    } catch (error) {
      return requestHandler.sendError(request, response, error);
    }

    //Fallback Error
    return response.send(appMessages.error.someError);
  }

  //🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹
}

module.exports = UserController;
