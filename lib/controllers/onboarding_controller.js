const JWTToken = require("../Utilities/jwt_token.js");
const Logger = require("../Utilities/logger.js");

const { emit } = require("nodemon");
var otpGenerator = require("otp-generator");
const RequestHandler = require("../Utilities/error_handling/RequestHandler");
const { CustomError } = require("../Utilities/error_handling/errors.js");
const appMessages = require("../commons/app_messages.js");
const sequelizeORM = require("../Database/index.js");
const { where } = require("sequelize");
const { sendOTP } = require("../Utilities/EmailManager.js");
const {
  addMinutesToDate,
  isValidString,
} = require("../Utilities/utility_methods.js");
const { updateOrCreate } = require("../Database/models.ext.js");

const logger = new Logger();
const requestHandler = new RequestHandler(logger);

class OnboardingController {
  //🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹 LOGIN 🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹
  static async login(request, response) {
    try {
      //🌹🌹🌹 Check Request Have Email 🌹🌹🌹
      const email = request.body.email;
      if (email && typeof email == "string" && email != "") {
        //🌹🌹🌹Check If User Exist Or Not 🌹🌹🌹
        let user = await sequelizeORM.models.user.findOne({
          where: {
            email: email,
          },
          //include: [sequelizeORM.models.otp],
        });

        //🌹🌹🌹 Create User If Not Exist 🌹🌹🌹
        if (!user) {
          console.log("🌹🌹🌹 CREATING USER 🌹🌹🌹");
          user = await sequelizeORM.models.user.create({
            email: email,
            //include: [sequelizeORM.models.otp],
          });
        } else {
          console.log("🌹🌹🌹 USER ALREADY EXIST 🌹🌹🌹");
        }

        //🌹🌹🌹 Generate OTP and Update OTP Table🌹🌹🌹
        console.log("🌹🌹🌹 GENERATE OTP EMAIL 🌹🌹🌹");
        const otp = otpGenerator.generate(6, {
          digits: true,
          lowerCaseAlphabets: false,
          upperCaseAlphabets: false,
          specialChars: false,
        });
        const now = new Date();
        const expiration_time = addMinutesToDate(now, 5.0);

        const otp_instance = await updateOrCreate(
          sequelizeORM.models.otp,
          {
            userId: user.id,
          },
          {
            otp: otp,
            expiration_time: expiration_time,
            userId: user.id,
          }
        );

        var verification_key = {
          timestamp: now,
          otp_id: otp_instance.id,
        };

        //🌹🌹🌹 Send Email 🌹🌹🌹
        console.log("🌹🌹🌹 SENDING EMAIL 🌹🌹🌹");
        const wordArr = email.split("@");
        const name = wordArr.length > 0 ? wordArr[0] : email;
        sendOTP(email, name, otp);

        //🌹🌹🌹 Success 🌹🌹🌹
        return requestHandler.sendSuccess(
          response,
          appMessages.success.verifyEmailSent
        )({ user, verification_key });
      } else {
        return requestHandler.sendCustomError(
          request,
          response,
          CustomError.BadRequest(appMessages.error.emailMissing)
        );
      }
    } catch (error) {
      return requestHandler.sendError(request, response, error);
    }

    //Fallback Error
    return response.send(appMessages.error.someError);
  }

  //🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹🌹

  static async verifyAccount(request, response) {
    try {
      //🌹🌹🌹 Check Request Have OTP ID 🌹🌹🌹
      const otp_id = request.body.otp_id;
      const otp = request.body.otp;

      //🌹🌹🌹 CHECK OTP ID MISSING 🌹🌹🌹
      if (!isValidString(otp_id)) {
        return requestHandler.sendCustomError(
          request,
          response,
          CustomError.BadRequest(appMessages.error.missingOTP_ID)
        );
      }

      //🌹🌹🌹 CHECK OTP MISSING 🌹🌹🌹
      if (!isValidString(otp)) {
        return requestHandler.sendCustomError(
          request,
          response,
          CustomError.BadRequest(appMessages.error.missingOTP)
        );
      }

      //🌹🌹🌹 Check If OTP RECORD EXIST FOR OTP ID 🌹🌹🌹
      let otp_instance = await sequelizeORM.models.otp.findOne({
        where: {
          id: otp_id,
        },
      });

      if (!otp_instance) {
        return requestHandler.sendCustomError(
          request,
          response,
          CustomError.BadRequest(appMessages.error.invalidOtpID)
        );
      }

      //🌹🌹🌹 CHECK EXPIRING DATE 🌹🌹🌹
      const validTime = otp_instance.expiration_time - Date.now();
      if (validTime < 0) {
        return requestHandler.sendCustomError(
          request,
          response,
          CustomError.BadRequest(appMessages.error.expiredOTP)
        );
      }

      //🌹🌹🌹 MATCH OTP 🌹🌹🌹
      if (otp_instance.otp != otp) {
        return requestHandler.sendCustomError(
          request,
          response,
          CustomError.BadRequest(appMessages.error.invalidOTP)
        );
      }

      //🌹🌹🌹 UPDATE USER TABLE TO VERIFIED 🌹🌹🌹
      await sequelizeORM.models.user.update(
        { isVerified: 1 },
        { where: { id: otp_instance.userId } }
      );

      //🌹🌹🌹 FTECH USER TABLE TO CREATE TOKEN 🌹🌹🌹
      const user = await sequelizeORM.models.user.findByPk(otp_instance.userId);
      const userObject = { id: user.id, email: user.email };

      //🌹🌹🌹 Success 🌹🌹🌹
      return requestHandler.sendSuccess(
        response,
        appMessages.success.loginSuccess
      )({
        ...user.dataValues,
        token: JWTToken.createJWT(userObject),
      });
    } catch (error) {
      return requestHandler.sendError(request, response, error);
    }

    //Fallback Error
    return response.send(appMessages.error.someError);
  }
}

module.exports = OnboardingController;
