const express = require("express");
const UserController = require("../../controllers/user_controller");
const router = express.Router();

router.post("/complete_profile", async (request, response) => {
  return UserController.completeProfile(request, response);
});

router.get("/user", async (request, response) => {
  return UserController.getUser(request, response);
});

router.get("/users", async (request, response) => {
  return UserController.getUsers(request, response);
});

module.exports = router;
