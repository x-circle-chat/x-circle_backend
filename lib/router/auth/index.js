const router = require("express").Router();

router.use(require("./user_route.js"));

module.exports = router;
