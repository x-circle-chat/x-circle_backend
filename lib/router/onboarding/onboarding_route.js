const express = require("express");
const OnboardingController = require("../../controllers/onboarding_controller");
const router = express.Router();

router.post("/login", async (request, response) => {
  return OnboardingController.login(request, response);
});

router.post("/verify", async (request, response) => {
  return OnboardingController.verifyAccount(request, response);
});

module.exports = router;
