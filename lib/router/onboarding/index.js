const router = require("express").Router();

router.use("/", require("./onboarding_route.js"));

module.exports = router;
