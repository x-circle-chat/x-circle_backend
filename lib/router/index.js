const router = require("express").Router();

router.use("/", require("./onboarding"));
router.use("/auth", require("./auth"));

module.exports = router;
