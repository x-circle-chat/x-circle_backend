const path = require("path");
require("dotenv").config();

// config.js
module.exports = {
  host: process.env.HOST_ADDRESS,
  version: process.env.VERSION,
  env: process.env.NODE_ENV,
  app: {
    port: process.env.DEV_APP_PORT,
    appName: process.env.APP_NAME,
    get base_url() {
      return `http://${this.host}:${this.port}`;
    },
  },
  dbConfig: {
    host: this.host,
    database: "X_CIRCLE",
    user: "x_circle_admin",
    password: "x_circle_admin_123",
  },
  auth: {
    jwt_header_key: "Authorization",
    jwt_secret: "x_circle_x_circle_123",
  },
  public: {
    get images() {
      return path.resolve(global.__basedir, "./lib/public/images");
    },
    get uploadPath() {
      return path.resolve(global.__basedir, "./lib/public/uploads");
    },
    get web() {
      return path.resolve(global.__basedir, "./lib/public/web");
    },
    get emails() {
      return path.resolve(global.__basedir, "./lib/public/emails");
    },
  },
  Init: function () {
    this.app.host = this.host;
    this.dbConfig.host = this.host;
    delete this.Init;
    return this;
  },
}.Init();
